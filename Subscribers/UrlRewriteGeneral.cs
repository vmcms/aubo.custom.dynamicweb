﻿using Dynamicweb.Extensibility.Notifications;
using Dynamicweb.Notifications;
using System.Web;
using System.Net;
using Dynamicweb.Logging;

namespace AUBO.Subscribers
{
    [Subscribe(Standard.Application.AfterBeginRequest)]
    public class UrlRewriteGeneral : NotificationSubscriber
    {
        /// <summary>
        /// check if URL contains double dash. Then redirect to single dash.
        /// Universal redirect type for a small period of time after launch of DW9
        /// </summary>
        /// <param name="notification"></param>
        /// <param name="args"></param>
        public override void OnNotify(string notification, NotificationArgs args)
        {
            Standard.Application.AfterBeginRequestArgs loadedArgs = args as Standard.Application.AfterBeginRequestArgs;

            var rawUri = HttpContext.Current.Request.RawUrl;
            var referer = HttpContext.Current.Request.UrlReferrer;

            bool isRealPage = false;
            if (HttpContext.Current.Request.CurrentExecutionFilePathExtension == "")
            {
                isRealPage = true;
            }

            bool pathContainsDoubleDash = rawUri.Contains("--");
            bool pathContainsTrippleDash = rawUri.Contains("---");
            if (pathContainsDoubleDash && isRealPage && !pathContainsTrippleDash)
            {
                string urlRedirect = rawUri.Replace("--", "-");

                bool pageExist = this.RemoteFileExists(urlRedirect);
                if (pageExist == false)
                {
                    //init logger
                    var logger = LogManager.Current.GetLogger("AUBO_URL");
                    logger.Log(string.Format("AUBO URL ISSUE ~ {0} ~ {1} ~ {2}", rawUri, urlRedirect, referer));

                    //redirect
                    HttpContext.Current.Response.RedirectPermanent(urlRedirect, true);
                }

            }

        }

        ///
        /// Checks the file exists or not.
        ///
        /// The URL of the remote file.
        /// True : If the file exits, False if file not exists
        private bool RemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                response.Close();
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }

    }
}
